//
//  UserPreview.h
//  firststep
//
//  Created by Donovan Charpin on 03/09/13.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "User.h"
#import "Util.h"


@interface UserPreview : UIView

@property (nonatomic, retain) UIImageView* background;
@property (nonatomic, retain) UILabel* userNameLabel;
@property (nonatomic, retain) UILabel* ageNameLabel;
@property (nonatomic, retain) UILabel* cityNameLabel;
@property (nonatomic, retain) UIButton* winkButton;
@property (nonatomic, retain) CALayer* profilePicture;
@property (nonatomic, retain) UIButton* buttonFastProfile;
@property (nonatomic, retain) UILabel* countDown;
@property BOOL viewExpanded;
@property BOOL winkSend;
@property User* userBox;
@property int defaultpositionX;
@property int counter;
@property PFObject *couplePeek;


-(id) initWithUser:(User*) user withPositionX:(int)positionX andY:(int)positionY withPosition:(BOOL)position;

-(IBAction) fastProfile :(id)sender;

-(IBAction) winkPerson :(id)sender;

- (IBAction)startCountdown:(id)sender;

@end
