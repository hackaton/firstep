//
//  ViewController.m
//  firststep
//
//  Created by Donovan Charpin on 25/04/13.
//
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import <AddressBook/AddressBook.h>
#import "TargetConditionals.h"
#import "User.h"
#import "MTPopupWindow.h"
#import "ProfileViewController.h"
#import "ChatViewController.h"
#import "SettingsViewController.h"
#import "AddCreditViewController.h"
#import "Util.h"
#import "UserPreview.h"
#import "PBFlatSegmentedControl.h"
#import "RMSwipeTableView.h"
#import "ConversationListViewController.h"


@interface ViewController () <  FBFriendPickerDelegate,
                                FBPlacePickerDelegate,
                                CLLocationManagerDelegate,
                                UIActionSheetDelegate> {
    @private int _retryCount;
}

/* Facebook attributes */
@property (strong, nonatomic) FBUserSettingsViewController *settingsViewController;
@property (strong, nonatomic) IBOutlet FBProfilePictureView *userProfileImage;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) FBCacheDescriptor *placeCacheDescriptor;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) UIPopoverController *popover;
@property (nonatomic) CGRect popoverFromRect;
/* End facebook attributes */

@end

@implementation ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.hidesBackButton = YES;
    }
    return self;
    
}

- (void)enableUserInteraction:(BOOL) enabled {
    if (enabled) {
        [self.activityIndicator stopAnimating];
    } else {
        [self centerAndShowActivityIndicator];
    }
    
    [self.view setUserInteractionEnabled:enabled];
}// Helper method to request publish permissions and post.


- (void) presentAlertForError:(NSError *)error {
    // Facebook SDK * error handling *
    // Error handling is an important part of providing a good user experience.
    // When fberrorShouldNotifyUser is YES, a fberrorUserMessage can be
    // presented as a user-ready message
    if (error.fberrorShouldNotifyUser) {
        // The SDK has a message for the user, surface it.
        [[[UIAlertView alloc] initWithTitle:@"Something Went Wrong"
                                    message:error.fberrorUserMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    } else {
        NSLog(@"unexpected error:%@", error);
    }
}

#pragma mark - UI Behavior


- (void)centerAndShowActivityIndicator {
    CGRect frame = self.view.frame;
    CGPoint center = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
    self.activityIndicator.center = center;
    [self.activityIndicator startAnimating];
}

// Displays the user's name and profile picture so they are aware of the Facebook
// identity they are logged in as.
- (void)populateUserDetails {
    if (FBSession.activeSession.isOpen) {
        NSLog(@"Facebook permissions: %@",FBSession.activeSession.permissions);

        /* Facebook request */
        FBRequest *friendRequest = [FBRequest requestForGraphPath:@"/me/?fields=gender,interested_in,relationship_status,email,first_name,last_name,location,id,birthday"];
        
        [friendRequest startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            /* Print facebook result */
            NSLog(@"result: %@", result);
            
            if (!error) {
                NSString* credits = @"20";
                
                //Récupération du delegate pour pouvoir setter l'utilisateur courant ensuite
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                
                self.userProfileImage.profileID = [result objectForKey:@"id"];

                /* Query to check whether the user exists */
                PFQuery *userExists = [PFQuery queryWithClassName:@"Users"];
                [userExists whereKey:@"idFacebook" equalTo:[result id]];
                [userExists getFirstObjectInBackgroundWithBlock:
                 
                ^(PFObject *object, NSError *error) {
                    if (appDelegate.connectedUser == nil) {
                        if (!object) {
                            //On crée un nouvel utilisateur avec toutes les infos récupérées de Facebook
                            appDelegate.connectedUser = [[User alloc] initWithAllInformation:[result first_name]
                                                                name:[result last_name]
                                                                birthday:[result birthday]
                                                                city:[[result objectForKey:(@"location")] objectForKey:(@"name")]
                                                                email:[result email]
                                                                idFacebook:[result id]
                                                                gender:[result objectForKey:(@"gender")]
                                                                interestedBy:@"à remplir"
                                                                bio:@"No description"
                                                                credits:credits];
                            //On sauvegarde la photo de profile pour pouvoir la récupérer ensuite
                            [appDelegate.connectedUser set_profileImage:self.userProfileImage];
                        } else {
                            // On instancie un objet qui contient les infos de l'utilisateur
                            appDelegate.connectedUser = [[User alloc] initFromDatabase:[result id]];
                            
                            //On sauvegarde la photo de profile pour pouvoir la récupérer ensuite
                            [appDelegate.connectedUser set_profileImage:self.userProfileImage];
                        }
                        // On ajoute les crédit dans la barre
                        [_valueCreditPeek setTitle:[[NSNumber numberWithInt:appDelegate.connectedUser._credits] stringValue] forState:UIControlStateNormal];
                    }
                }];
            }
        }];   

        CALayer *imageLayer = self.userProfileImage.layer;
        [imageLayer setCornerRadius:50];
        [imageLayer setBorderWidth:5];
        CGColorRef orangeColor = CGColorRetain([Util getTransparentColor].CGColor);
        [imageLayer setBorderColor:orangeColor];
        [imageLayer setMasksToBounds:YES];
    }
}

#pragma mark - Overrides

- (void)dealloc {
    _locationManager.delegate = nil;
    _settingsViewController.delegate = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"FirstStep";
    // Get the CLLocationManager going.
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    // We don't want to be notified of small changes in location, preferring to use our
    // last cached results, if any.
    self.locationManager.distanceFilter = 50;
    
    FBCacheDescriptor *cacheDescriptor = [FBFriendPickerViewController cacheDescriptor];
    [cacheDescriptor prefetchAndCacheForSession:FBSession.activeSession];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicator];
    
    //ajout du bouton à la photo de profil
    UIButton *nextBut=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    [nextBut addTarget:self action:@selector(nextbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.userProfileImage addSubview:nextBut];
    
    //ajout de l'évènement à ce bouton
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selfRoundPicture:)];
    [singleTap setNumberOfTapsRequired:1];
    [self.userProfileImage addGestureRecognizer:singleTap];
    
    // menu
    NSArray *itemArray = [NSArray arrayWithObjects: [UIImage imageNamed:@"chatMenuHighLight.png"], [UIImage imageNamed:@"profilMenuHighLight.png"], [UIImage imageNamed:@"reglagesMenuHighLight.png"], [UIImage imageNamed:@"notificationsMenuHighLight.png"], nil];
    _buttonMenu = [[PBFlatSegmentedControl alloc] initWithItems:itemArray];
    _buttonMenu.frame = CGRectMake(20, 5, 280, 40);
    _buttonMenu.segmentedControlStyle = UISegmentedControlStylePlain;
    [_buttonMenu setSelectedSegmentIndex:UISegmentedControlNoSegment];
    [_buttonMenu addTarget:self
                    action:@selector(ButtonSegmentedPressed:)
          forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_buttonMenu];
    [self displayUsersAround];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (FBSession.activeSession.isOpen) {
        self.navigationController.navigationBarHidden = YES; //suppression barre
        [self populateUserDetails];
    }
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void) viewDidAppear:(BOOL)animated {
    if (FBSession.activeSession.isOpen) {
        [self.locationManager startUpdatingLocation];
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - FBUserSettingsDelegate methods

- (void)loginViewControllerDidLogUserOut:(id)sender {
    // Facebook SDK * login flow *
    // There are many ways to implement the Facebook login flow.
    // In this sample, the FBLoginView delegate (SCLoginViewController)
    // will already handle logging out so this method is a no-op.
}

- (void)loginViewController:(id)sender receivedError:(NSError *)error{
    // Facebook SDK * login flow *
    // There are many ways to implement the Facebook login flow.
    // In this sample, the FBUserSettingsViewController is only presented
    // as a log out option after the user has been authenticated, so
    // no real errors should occur. If the FBUserSettingsViewController
    // had been the entry point to the app, then this error handler should
    // be as rigorous as the FBLoginView delegate (SCLoginViewController)
    // in order to handle login errors.
    if (error) {
        NSLog(@"Unexpected error sent to the FBUserSettingsViewController delegate: %@", error);
    }
}


// Récupère latitude et longitude a chaque Update de position
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    NSNumber *latitude = [NSNumber numberWithDouble:newLocation.coordinate.latitude];
    NSNumber *longitude = [NSNumber numberWithDouble:newLocation.coordinate.longitude];
    NSLog(@"Latitude: %@, longitude: %@\n", latitude, longitude);
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.connectedUser._location = newLocation;
    [appDelegate.connectedUser saveUserToDatabase];
    [appDelegate.connectedUser getUsersAround];
    [self displayUsersAround];
}

// En cas d'erreur, mode avion etc..
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
}

/*
 * Animations Donovan
 */
- (IBAction)selfRoundPicture:(id)sender {
    [UIView animateWithDuration:.3 animations:^() {
        _valueCreditPeek.alpha = 1.0;
        _labelPeeks.alpha = 1.0;
    }];
    
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(aplhaTo0:) userInfo:nil repeats:NO];
}

- (void)aplhaTo0:(id)sender {
    [UIView animateWithDuration:1 animations:^() {
        _valueCreditPeek.alpha = 0;
        _labelPeeks.alpha = 0;
    }];
}

-(void) displayUsersAround{
    // Affichage des utilisateurs autour de soi
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.connectedUser getUsersAround];
    NSMutableArray *arrayUsers = appDelegate.connectedUser._usersAround;

    //Size Screen
    CGRect totalScreenRect = [[UIScreen mainScreen] bounds];
    //CGFloat screenWidth = screenRect.size.width;
    CGFloat totalScreenHeight = totalScreenRect.size.height;
    
    // Size UIScrollView
    CGRect screenRect = [_scrollerViewPerson bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    int offsetX = 20; // Offset en x de la box de la personne dans le scroller
    int offsetY = screenHeight-20; // Offset en y de la box de la personne dans le scroller
    int i = 0;
    
    // On supprime l'ancienne vue
    for (UILabel *subview in _scrollerViewPerson.subviews) {
        [subview removeFromSuperview];
        _scrollerViewPerson.contentSize = CGSizeMake(320, 0);
    }
    
    float height = _scrollerViewPerson.contentSize.height+80;

    for (User *user in arrayUsers) {
        NSLog(@"User around: %@", user._name);
        
        UserPreview *box = [[UserPreview alloc] initWithUser:user withPositionX:offsetX andY:(screenHeight-offsetY) withPosition:(BOOL)(i%2)];
        [_scrollerViewPerson addSubview:box];

        
        // On prépare le prochain label en modifiant sa hauteur
        height = height + 100;
        
        // On modifie la heuteur à l'interieur du scroller
        _scrollerViewPerson.contentSize = CGSizeMake(320, height);
        
        if(totalScreenHeight > 480){ // iPhone 5
            if(i<4){
                CGRect rect = CGRectMake(0, totalScreenHeight-150-height, 320, height);
                [_scrollerViewPerson setFrame: rect];
                _scrollerViewPerson.bounces=NO; // Effet de rebondissement à false
            }else{
                _scrollerViewPerson.bounces=YES;
            }
        }
        else{ // iPhone4, 4S etc
            if(i<3){
                CGRect rect = CGRectMake(0, totalScreenHeight-150-height, 320, height);
                [_scrollerViewPerson setFrame: rect];
                _scrollerViewPerson.bounces=NO;
            }else{
                _scrollerViewPerson.bounces=YES;
            }
        }
        
        offsetY = offsetY-100;
        
        if(i%2 == 0){ // On place le label une fois à droite, une fois à gauche..
            offsetX = (screenWidth/2)-20;
        }else{
            offsetX = 20;
        }
        i++;
    }
    // On place le scroll en bas du UIScrollView
    CGPoint bottomOffset = CGPointMake(0, _scrollerViewPerson.contentSize.height - _scrollerViewPerson.bounds.size.height);
    [_scrollerViewPerson setContentOffset:bottomOffset animated:YES];
}

- (UIImage*)getUserImageFromFBView{
    for (NSObject *obj in [self.userProfileImage subviews]) {
        if ([obj isMemberOfClass:[UIImageView class]]) {
            UIImageView *objImg = (UIImageView *)obj;
            return objImg.image;
        }
    }
    return nil;
}

-(void)ButtonSegmentedPressed:(UISegmentedControl*)sender{
        
    NSInteger selectedSegment = sender.selectedSegmentIndex;

    [sender setImage:[UIImage imageNamed:@"chatMenuHighLight.png"] forSegmentAtIndex:0];
    [sender setImage:[UIImage imageNamed:@"profilMenuHighLight.png"] forSegmentAtIndex:1];
    [sender setImage:[UIImage imageNamed:@"reglagesMenuHighLight.png"] forSegmentAtIndex:2];
    [sender setImage:[UIImage imageNamed:@"notificationsMenuHighLight.png"] forSegmentAtIndex:3];
    
    if (selectedSegment == 0) { // Quand on appuie, on peut modifier l'image
        [sender setImage:[UIImage imageNamed:@"chatMenuHighLight.png"] forSegmentAtIndex:sender.selectedSegmentIndex];
        ConversationListViewController *view=[[ConversationListViewController alloc] initWithNibName:@"ConversationListViewController" bundle:nil];
        [self.navigationController pushViewController:view animated:YES];
        
    }else if(selectedSegment == 1){
        [sender setImage:[UIImage imageNamed:@"profilMenuHighLight.png"] forSegmentAtIndex:sender.selectedSegmentIndex];
        /* Récupération du user courrant */
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        User* profil = appDelegate.connectedUser;
        
        ProfileViewController *view=[[ProfileViewController alloc] initWithUser:profil];
        [self.navigationController pushViewController:view animated:YES];
    }else if(selectedSegment == 2){
        [sender setImage:[UIImage imageNamed:@"reglagesMenuHighLight.png"] forSegmentAtIndex:sender.selectedSegmentIndex];
        SettingsViewController *view=[[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil fbUser:_settingsViewController];
        [self.navigationController pushViewController:view animated:YES];
    }else if(selectedSegment == 3){
        [sender setImage:[UIImage imageNamed:@"notificationsMenuHighLight.png"] forSegmentAtIndex:sender.selectedSegmentIndex];
        // Ajouter la vue lorsqu'elle sera créé
        ChatViewController *view=[[ChatViewController alloc] initWithNibName:@"ChatViewController" bundle:nil];
        [self.navigationController pushViewController:view animated:YES];

        //[self.navigationController pushViewController:view animated:YES];
    }
    sender.selectedSegmentIndex =  UISegmentedControlNoSegment;
}

- (IBAction)addCreditView:(id)sender {
    AddCreditViewController *view=[[AddCreditViewController alloc] initWithNibName:@"AddCreditViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:YES];

}
@end
