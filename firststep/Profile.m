//
//  Profile.m
//
//  Created by Morgan BRASSEUR on 18/05/13.
//  Copyright (c) 2013 Morgan BRASSEUR. All rights reserved.
//

#import "Profile.h"

@implementation Profile

// METHODS
- (id) initWithAll	: (NSString*)description
					: (LifeFeatures*)lifeFeatures
					: (PhysicalFeatures*)physicalFeatures
{
	self = [super init];
    
    if(!self)
    {
        return nil;
    }
    self._description = description;
    self._lifeFeatures = lifeFeatures;
    self._physicalFeatures = physicalFeatures;
    
    return self;
}

- (id) init {
    self = [super init];
    
    if(!self)
    {
        return nil;
    }
    self._description = @"";
    self._lifeFeatures = [[LifeFeatures alloc] init];
    self._physicalFeatures = [[PhysicalFeatures alloc] init];
    
    return self;
}

// PROTOCOL OVERRIDES
- (DatabaseResult*) Save
{
    return kOk;
}

- (DatabaseResult*) Load
{
    return kOk;
}

// GETTERS
- (NSString*) get_description
{
    return _description;
}

- (LifeFeatures*) get_lifeFeatures
{
    return _lifeFeatures;
}

- (PhysicalFeatures*) get_physicalFeatures
{
    return _physicalFeatures;
}

// SETTERS
- (void) set_description : (NSString*)value
{
    self._description = value;
}

- (void) set_lifeFeatures : (LifeFeatures*)value
{
    self._lifeFeatures = value;
}

- (void) set_physicalFeatures : (PhysicalFeatures*)value
{
    self._physicalFeatures = value;
}

@end
