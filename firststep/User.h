//
//  myProfil.h
//  Essai
//
//  Created by Donovan Charpin on 14/05/13.
//  Copyright (c) 2013 Donovan Charpin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>
#import <Parse/Parse.h>

@interface User : NSObject

/*
 *Propriétés accessibles directement : object.property
 */
@property (nonatomic, retain) NSString *_idProfil;
@property (nonatomic, retain) NSString *_firstName;
@property (nonatomic, retain) NSString *_name;
@property (nonatomic, retain) NSString *_birthday;
@property (nonatomic, retain) NSString *_city;
@property (nonatomic, retain) NSString *_email;
@property (nonatomic, retain) NSString *_idFacebook;
@property (nonatomic, retain) NSString *_gender;
@property (nonatomic, retain) NSString *_interestedBy;
@property (nonatomic, retain) NSString *_bio;
@property (nonatomic, retain) FBProfilePictureView *_profileImage;
@property (nonatomic) int _age;
@property (nonatomic) int _credits;
@property (nonatomic, retain) CLLocation *_location;
@property (nonatomic, retain) PFObject *_databaseUser;
@property (nonatomic, retain) NSMutableArray *_usersAround;
@property (nonatomic, retain) UIImage *_profilePicture;

/*
 * Initialise un utilisateur
 * à partir de ses informations
 * préalablement obtenues de Facebook
 */
- (id) initWithAllInformation: (NSString *)firstName
                        name : (NSString *)name
                    birthday : (NSString *)birthday
                        city : (NSString *)city
                       email : (NSString *)email
                  idFacebook : (NSString *)idFacebook
                      gender : (NSString *)gender
                interestedBy : (NSString *)interestedBy
                         bio : (NSString *)bio
                     credits : (NSString *)credits;

/*
 * Initialise un utilisateur
 * à partir des informations de la base de donnée
 */
-(id) initFromDatabase : (NSString *)idFacebookValue;

/*
 * Permet la sauvegarde ou la mise à jour
 * d'un utilisateur dans la base de données
 */
-(void) saveUserToDatabase;

/*
 * Lance une requête permettant de récupérer les utilisateurs 
 * au tour de l'utilisateur courrant
 */
-(NSArray *) getUsersAround;

/*
 * Méthode qui remplit les attributs de l'objet utilisateur courant 
 * à partir de l'objet PFObject renvoyé par la base de donnée
 * Seulement utilisé pour traiter les réponses des requêtes vers la base
 */
-(void) setUser: (User *)user fromArray: (PFObject *)obj;

/* 
 * Renvoie la photo de profil Facebook d'un utilisateur
 * À ne pas utiliser pour récupérer la photo de 
 * l'utilisateur actuellement connecté
 * (plutôt utiliser l'attribut _profileImage)
 */
-(UIImage *) getUserImage;

@end
