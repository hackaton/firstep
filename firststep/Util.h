//
//  Util.h
//  firststep
//
//  Created by Stéphane Bruckert on 5/16/13.
//
//

#import <Foundation/Foundation.h>

@interface Util : NSObject

+ (NSNumber*)getAgeFromString:(NSString *) dateOfBirth;
+ (CALayer*)getImage:(UIImage*)image withDimensions:(CGRect)rect surroundedWithColor:(UIColor*)color withRadius:(int)radius;
+ (UIColor*)getBlueColor;
+ (UIColor*)getTransparentColor;
+ (UIColor*)getOrangeColor;
+ (UIColor*)getWhiteColor;

@end

