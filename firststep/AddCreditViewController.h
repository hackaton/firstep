//
//  AddCreditViewController.h
//  firststep
//
//  Created by Donovan Charpin on 08/07/13.
//
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>


@interface AddCreditViewController : UIViewController

- (IBAction)backButton:(id)sender;

- (IBAction)shareOnFacebook:(id)sender;

@end
