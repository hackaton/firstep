//
//  AppDelegate.h
//  firststep
//
//  Created by Donovan Charpin on 25/04/13.
//
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "User.h"

@class ViewController;
@class LoginViewController;


@interface AppDelegate : UIResponder <UIApplicationDelegate, UINavigationControllerDelegate>{
    UIImageView *zView;
    UIImageView *fView;
    
    UIView *rView;//UIView
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@property (strong, nonatomic) ViewController *mainViewController;

@property (strong, nonatomic) LoginViewController* loginViewController;

@property BOOL isNavigating;

@property (strong, nonatomic) User* connectedUser;

@end