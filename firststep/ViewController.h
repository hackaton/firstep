//
//  ViewController.h
//  firststep
//
//  Created by Donovan Charpin on 25/04/13.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <FacebookSDK/FacebookSDK.h>
#import <CoreLocation/CoreLocation.h>
#import "User.h"

@interface ViewController : UIViewController <FBUserSettingsDelegate>

// remet l'alpha à 0
- (void)aplhaTo0:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *valueCreditPeek;

- (IBAction)addCreditView:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *labelPeeks;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollerViewPerson;

@property (weak, nonatomic) IBOutlet UIImageView *contentMenu;

/* List */
@property (strong, nonatomic) UISegmentedControl *buttonMenu;

-(void) displayUsersAround;



@end
