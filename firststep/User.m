//
//  myProfil.m
//  Essai
//
//  Created by Donovan Charpin on 14/05/13.
//  Copyright (c) 2013 Donovan Charpin. All rights reserved.
//

#import "User.h"
#import "Util.h"

@implementation User

@synthesize _idProfil;
@synthesize _firstName;
@synthesize _name;
@synthesize _birthday;
@synthesize _city;
@synthesize _email;
@synthesize _idFacebook;
@synthesize _gender;
@synthesize _interestedBy;
@synthesize _bio;
@synthesize _profileImage;
@synthesize _age;
@synthesize _credits;
@synthesize _location;
@synthesize _databaseUser;
@synthesize _usersAround;
@synthesize _profilePicture;


- (id) initWithAllInformation: (NSString *)firstName name : (NSString *)name
                    birthday : (NSString *)birthday city : (NSString *)city
                       email : (NSString *)email idFacebook : (NSString *)idFacebook
                      gender : (NSString *)gender interestedBy : (NSString *)interestedBy
                         bio : (NSString *)bio credits : (NSString *)credits {
    self = [super init];
    if (!self) return nil;

    NSNumber *age = [Util getAgeFromString:(birthday)];
    self._firstName = firstName;
    self._name = name;
    self._birthday = birthday;
    self._city = city;
    self._email = email;
    self._idFacebook = idFacebook;
    self._gender = gender;
    self._interestedBy = interestedBy;
    self._bio = bio;
    self._age = [age intValue];
    self._credits = [credits intValue];
    _databaseUser = [PFObject objectWithClassName:@"Users"];
    [self saveUserToDatabase];
    return self;
}

-(id) initFromDatabase : (NSString *)idFacebookValue {
    self = [super init];
    if (!self) return nil;
    
    PFQuery *query = [PFQuery queryWithClassName:@"Users"];
    [query whereKey:@"idFacebook" equalTo:idFacebookValue];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSLog(@"%@", objects);
        [self setUser:self fromArray: [objects objectAtIndex: 0]];
    }];
    
    //PFObject* firstObject = [query getFirstObject];
    //NSString *objectId = [firstObject objectId];
    //PFGeoPoint *userGeoPoint = [self objectForKey:@"currentPosition"];
    //PFObject *databaseUser = [query getObjectWithId:objectId];
    //_databaseUser = databaseUser;
    
    return self;
}

-(void) saveUserToDatabase {
    PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLocation: _location];
    
    [_databaseUser setObject:_firstName forKey:@"firstName"];
    [_databaseUser setObject:_name forKey:@"name"];
    [_databaseUser setObject:_email forKey:@"email"];
    [_databaseUser setObject:_gender forKey:@"gender"];
    [_databaseUser setObject:_city forKey:@"city"];
    [_databaseUser setObject:_idFacebook forKey:@"idFacebook"];
    [_databaseUser setObject:_birthday forKey:@"birthday"];
    [_databaseUser setObject:_bio forKey:@"bio"];
    [_databaseUser setObject:[Util getAgeFromString:(_birthday)] forKey:@"age"];
    [_databaseUser setObject:[NSNumber numberWithInt:_credits] forKey:@"credits"];
    [_databaseUser setObject:geoPoint forKey:@"currentPosition"];
    
    [_databaseUser saveInBackground];
}

-(NSArray *) getUsersAround{
    PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLocation: _location];
    PFQuery *query = [PFQuery queryWithClassName:@"Users"];
    [query whereKey:@"currentPosition" nearGeoPoint:geoPoint withinKilometers:5];
    [query whereKey:@"idFacebook" notEqualTo:_idFacebook];
    NSArray *usersAround = [query findObjects];
    
    /* On transforme notre array d'array en array d'utilisateurs */
    _usersAround = [[NSMutableArray alloc] init];
    for (PFObject* obj in usersAround) {
        User *user = [[User alloc] init];
        [self setUser:user fromArray: obj];
        [_usersAround addObject:user];
    }
    return _usersAround;
}

-(void) setUser: (User *)user fromArray: (PFObject *)obj {
    NSNumber *userAge = [obj objectForKey : @"age"];
    NSNumber *userCredit = [obj objectForKey : @"credits"];
    
    user._idProfil = [obj objectForKey : @"Users"];
    user._firstName = [obj objectForKey : @"firstName"];
    user._name = [obj objectForKey : @"name"];
    user._birthday = [obj objectForKey : @"birthday"];
    user._city = [obj objectForKey : @"city"];
    user._email = [obj objectForKey : @"email"];
    user._idFacebook = [obj objectForKey : @"idFacebook"];
    user._gender = [obj objectForKey : @"gender"];
    user._interestedBy = [obj objectForKey : @"interestedBy"];
    user._bio = [obj objectForKey : @"bio"];
    user._age = [userAge intValue];
    user._credits = [userCredit intValue];
    //[self set_locationWithLocation:userGeoPoint objectForKey : @"currentPosition"]];
}

-(UIImage *) getUserImage {
    NSString *urlPicture = [NSString stringWithFormat:@"%@%@%@", @"http://graph.facebook.com/", _idFacebook, @"/picture?width=200&height=200"];
    
    UIImage* profilePicture = [UIImage imageWithData:
                               [NSData dataWithContentsOfURL:
                                [NSURL URLWithString: urlPicture]]];
    _profilePicture = profilePicture;
    return _profilePicture;
}

@end