//
//  Profile.h
//
//  Created by Morgan BRASSEUR on 18/05/13.
//  Copyright (c) 2013 Morgan BRASSEUR. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum DatabaseResult : NSUInteger {
    kOk,
    kConnectionError,
    kSaveError,
    kLoadError
} DatabaseResult;

@protocol DatabaseStorable <NSObject>

// METHODS
- (DatabaseResult*) Save;
- (DatabaseResult*) Load;

@end