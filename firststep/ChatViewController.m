//
//  ChatViewController.m
//  firststep
//
//  Created by Donovan Charpin on 15/05/13.
//
//

#import "UIBubbleTableView.h"
#import "UIBubbleTableViewDataSource.h"
#import "NSBubbleData.h"
#import "Message.h"
#import "ChatViewController.h"
#import "AppDelegate.h"


@interface ChatViewController ()
{
    IBOutlet UIBubbleTableView*     bubbleTable;
    IBOutlet UIView*                textInputView;
    IBOutlet UITextField*           textField;
    IBOutlet UIButton*              buttonSend;

    NSMutableArray*                 bubbleData;
}
@end

@implementation ChatViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//VIEW METHODES

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    User* profil = appDelegate.connectedUser;
    NSLog(@"age: %@", profil._city);
    // Do any additional setup after loading the view from its nib.
    
    // Messages
    
    bubbleData = [[NSMutableArray alloc] init];
    
    self._lastDate = [NSDate dateWithTimeIntervalSinceNow:0];
    
    // Récupération de tous les messages dans la limite indiquée
    
    NSMutableArray* messages = [Message getAllMessages:(@"SmCSf36EDm") userReceiver:(@"hScozm3ZEG") limit:10];
    
    for(Message* message in messages)
    {
        NSBubbleOrigin bubbleOrigin = BUBBLE_FROM_SOMEBODY;
        if([message._userSender isEqual: (@"SmCSf36EDm")])
        {
            bubbleOrigin = BUBBLE_FROM_ME;
        }
        NSBubbleData* bubble = [NSBubbleData dataWithText:message._content date:message._date origin:bubbleOrigin];
        if([message._userSender isEqual: (@"SmCSf36EDm")])
        {
            bubble.avatar = [UIImage imageNamed:@"icon_114.png"];
        }
        else
        {
            bubble.avatar = [UIImage imageNamed:@"avatar1.png"];
        }
        [bubbleData addObject:bubble];
        self._lastDate = message._date;
    }
    
    bubbleTable.bubbleTableDataSource = self;
    bubbleTable.showAvatars = FALSE;
    
    //Now is typing!!!
    //bubbleTable.typingBubble = NSBubbleTypingTypeSomebody;
    
    [bubbleTable reloadData];
    
    //Scroll to bottom
    CGPoint offset = CGPointMake(0, bubbleTable.contentSize.height - bubbleTable.frame.size.height);
    [bubbleTable setContentOffset: offset animated:NO];
    
    // Keyboard events
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    // Button events
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(editingChanged:) name:UITextFieldTextDidChangeNotification object:textField];
    
    // Preparation de la réception des messages
    
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:
                                [self methodSignatureForSelector: @selector(timerCallback)]];
    [invocation setTarget:self];
    [invocation setSelector:@selector(timerCallback)];
    self._timer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                               invocation:invocation repeats:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    
    self.navigationController.navigationBarHidden = YES; //suppression barre
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillDisappear:animated];
}

- (IBAction)returnToFirstView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}


//METHODES

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - UIBubbleTableViewDataSource implementation

- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    return [bubbleData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    return [bubbleData objectAtIndex:row];
}

#pragma mark - Keyboard events

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = textInputView.frame;
        frame.origin.y -= kbSize.height;
        textInputView.frame = frame;
        
        frame = bubbleTable.frame;
        frame.size.height -= kbSize.height;
        bubbleTable.frame = frame;
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = textInputView.frame;
        frame.origin.y += kbSize.height;
        textInputView.frame = frame;
        
        frame = bubbleTable.frame;
        frame.size.height += kbSize.height;
        bubbleTable.frame = frame;
    }];
}

#pragma mark - Actions

- (IBAction)editingChanged:(id)sender
{
    if([textField.text length] > 0)
    {
        [buttonSend setEnabled:YES];
    }
    else
    {
        [buttonSend setEnabled:NO];
    }
}

- (IBAction)sayPressed:(id)sender
{
    NSDate* now = [NSDate dateWithTimeIntervalSinceNow:0];
    
    bubbleTable.typingState = TYPING_NOBODY;
    NSBubbleData *sayBubble = [NSBubbleData dataWithText:textField.text date:now origin:BUBBLE_FROM_ME];
    
    [bubbleData addObject:sayBubble]; //Il faudra ajouter la réponse SQL comme état du message
    Message* sendMessage = [[Message alloc] initWithAll:textField.text userSender:(@"SmCSf36EDm") userReceiver:(@"hScozm3ZEG") date:now];
    [sendMessage save];
    
    //Reset Text Field
    [textField setText: @""];
    [textField resignFirstResponder];
    
    //Reset Button
    [buttonSend setEnabled:FALSE];
    
    //Update Table View
    [bubbleTable reloadData];
    //Scroll to bottom
    CGPoint offset = CGPointMake(0, bubbleTable.contentSize.height - bubbleTable.frame.size.height);
    [bubbleTable setContentOffset: offset animated:YES];
}

- (void)timerCallback
{
    //[self._timer retain];
    // Récupération des nouveaux messages depuis la base de données
    NSMutableArray* newMessages = [Message getNewMessages:self._lastDate userSender:(@"hScozm3ZEG") userReceiver:(@"SmCSf36EDm")];
    
    // Affichage des messages récupérés
    for(Message* message in newMessages)
    {
        NSBubbleData* sayBubble = [NSBubbleData dataWithText:[message _content] date:message._date origin:BUBBLE_FROM_SOMEBODY];
        [bubbleData addObject:sayBubble]; //Il faudra ajouter la réponse SQL comme état du message
        
        // Mise à jour de la date de polling
        self._lastDate = message._date;
    }
    
    [bubbleTable reloadData];
    //[self._timer release];
}

- (IBAction)backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
