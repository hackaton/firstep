//
//  Util.m
//  firststep
//
//  Created by Stéphane Bruckert on 5/16/13.
//
//

#import "Util.h"
#import <QuartzCore/QuartzCore.h>

@implementation Util

+ (NSNumber*)getAgeFromString:(NSString *) dateOfBirth {
    //Convert dateOfBirth String to Date
    [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehaviorDefault];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    
    //Get age from birthdate
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateOfBirth];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    
    NSDateComponents *dateComponentsNow = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDateComponents *dateComponentsBirth = [calendar components:unitFlags fromDate:dateFromString];
    
    int age;
    if (([dateComponentsNow month] < [dateComponentsBirth month]) ||
        (([dateComponentsNow month] == [dateComponentsBirth month]) && ([dateComponentsNow day] < [dateComponentsBirth day])))
    {
        age = [dateComponentsNow year] - [dateComponentsBirth year] - 1;
    } else {
        age = [dateComponentsNow year] - [dateComponentsBirth year];
    }
    return [NSNumber numberWithInt:age];
}

+ (CALayer*)getImage:(UIImage*)image withDimensions:(CGRect)rect surroundedWithColor:(UIColor*)color withRadius:(int)radius {
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = rect;
    [layer setCornerRadius:radius];
    [layer setBorderWidth:5];
    layer.contents = (id)image.CGImage;
    CGColorRef colorRef = CGColorRetain(color.CGColor);
    [layer setBorderColor:colorRef];
    [layer setMasksToBounds:YES];
    return layer;
}

+ (UIColor*)getBlueColor {
    return [UIColor colorWithRed:1/3 green:0.6313 blue:0.9019 alpha:1];
}

+ (UIColor*)getTransparentColor {
    return [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
}

+ (UIColor*)getWhiteColor {
    return [UIColor colorWithRed:1 green:1 blue:1 alpha:0.9];
}

+ (UIColor*)getOrangeColor {
    return [UIColor colorWithRed:0.643 green:0.4039 blue:0.180 alpha:1];
}

@end