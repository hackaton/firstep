//
//  Profile.h
//
//  Created by Morgan BRASSEUR on 18/05/13.
//  Copyright (c) 2013 Morgan BRASSEUR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseStorable.h"
#import "LifeFeatures.h"
#import "PhysicalFeatures.h"
#import <Parse/Parse.h>

@interface Profile : NSObject <DatabaseStorable>
{
    // ATTRIBUTS
    NSString* _description;
    LifeFeatures* _lifeFeatures;
    PhysicalFeatures* _physicalFeatures;
}

// METHODS
- (id) initWithAll	: (NSString*)description
					: (LifeFeatures*)lifeFeatures
					: (PhysicalFeatures*)physicalFeatures;

// PROTOCOL OVERRIDES
- (DatabaseResult*) Save;
- (DatabaseResult*) Load;

// GETTERS
-(NSString*) get_description;
-(LifeFeatures*) get_lifeFeatures;
-(PhysicalFeatures*) get_physicalFeatures;

// SETTERS
-(void) set_description : (NSString*)value;
-(void) set_lifeFeatures : (LifeFeatures*)value;
-(void) set_physicalFeatures : (PhysicalFeatures*)value;

@end
