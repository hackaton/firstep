//
//  UserPreview.m
//  firststep
//
//  Created by Donovan Charpin on 03/09/13.
//
//

#import "UserPreview.h"
#import "AppDelegate.h"


@implementation UserPreview


-(id) initWithUser:(User*) user withPositionX:(int)positionX andY:(int)positionY withPosition:(BOOL)position
{
    self = [super init];
    if (!self) return nil;
    _viewExpanded = false;
    _winkSend = false;
    _defaultpositionX = positionX;
    self.userBox = user;
    if(position){ // Droite
        NSLog(@"%d",positionX);
        [self setFrame:CGRectMake(0, positionY, 320, 92)];
        // On crée le background en dessous du label
        UIImage *BgBox = [UIImage imageNamed:@"personneDroite.png"];
        _background = [[UIImageView alloc] initWithImage:BgBox];
        _background.alpha = 0.3;
        [_background setFrame:CGRectMake(positionX-10, 25, 170, 42)];
        [self addSubview:_background];
        
        // On crée le Label
        _userNameLabel = [ [UILabel alloc ] initWithFrame:CGRectMake(positionX-80, -5, 170, 42)];
        _userNameLabel.textAlignment =  UITextAlignmentRight;
        _userNameLabel.textColor = [UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:1.0f];
        _userNameLabel.backgroundColor = [UIColor clearColor];
        _userNameLabel.font = [UIFont fontWithName:@"Helvetica Neue Light" size:(13)];
        _userNameLabel.text = _userBox._firstName;
        [self addSubview:_userNameLabel];
        
        // wink button
        UIImage *winkImage = [UIImage imageNamed:@"winkButton.png"];
        _winkButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_winkButton setImage:winkImage forState:UIControlStateNormal];
        _winkButton.frame = CGRectMake(positionX, 25, 40, 42);
        _winkButton.backgroundColor = [UIColor clearColor];
        [_winkButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self addSubview:_winkButton];
        
        [_winkButton addTarget:self action:@selector(winkPerson:) forControlEvents:UIControlEventTouchUpInside];
        
        // On pose l'image de profil
        _profilePicture = [Util getImage:[_userBox getUserImage]
                          withDimensions:CGRectMake(positionX+90, 5, 80, 80)
                     surroundedWithColor:[Util getTransparentColor]
                              withRadius:40];
        [[self layer] addSublayer:_profilePicture];
        
        // fastProfile button
        _buttonFastProfile = [UIButton buttonWithType:UIButtonTypeCustom];
        _buttonFastProfile.frame = CGRectMake(positionX+90, 5, 80, 80);
        _buttonFastProfile.tag = 1;
        _buttonFastProfile.backgroundColor = [UIColor clearColor];
        _buttonFastProfile.layer.cornerRadius = 40; // this value vary as per your desire
        _buttonFastProfile.clipsToBounds = YES;
        [_buttonFastProfile setTitle:@"10:00" forState:UIControlStateNormal];
        _buttonFastProfile.font = [UIFont fontWithName:@"Helvetica Neue Light" size:(12)];
        [_buttonFastProfile setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        [self addSubview:_buttonFastProfile];

        
        [_buttonFastProfile addTarget:self action:@selector(fastProfile:) forControlEvents:UIControlEventTouchUpInside];
        
    }else{ // Gauche
        [self setFrame:CGRectMake(0, positionY, 320, 92)];
        // On crée le background en dessous du label
        UIImage *BgBox = [UIImage imageNamed:@"personneGauche.png"];
        _background = [[UIImageView alloc] initWithImage:BgBox];
        _background.alpha = 0.3;
        [_background setFrame:CGRectMake(positionX+10, 25, 170, 42)];
        [self addSubview:_background];
        
        // On crée le Label
        _userNameLabel = [ [UILabel alloc ] initWithFrame:CGRectMake(positionX+70, -5, 170, 42)];
        _userNameLabel.textAlignment =  UITextAlignmentLeft;
        _userNameLabel.textColor = [UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:1.0f];
        _userNameLabel.backgroundColor = [UIColor clearColor];
        _userNameLabel.font = [UIFont fontWithName:@"Helvetica Neue Light" size:(13)];
        _userNameLabel.text = _userBox._firstName;
        [self addSubview:_userNameLabel];
        
        // wink button
        _winkButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_winkButton setTitle:@"wink" forState:UIControlStateNormal];
        _winkButton.frame = CGRectMake(positionX+130, 25, 40, 42);
        _winkButton.font = [UIFont fontWithName:@"Helvetica Neue Light" size:(12)];
        _winkButton.backgroundColor = [UIColor clearColor];
        [_winkButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self addSubview:_winkButton];
        
        [_winkButton addTarget:self action:@selector(winkPerson:) forControlEvents:UIControlEventTouchUpInside];
        
        // On pose l'image de profil
        _profilePicture = [Util getImage:[_userBox getUserImage]
                          withDimensions:CGRectMake(positionX-10, 5, 80, 80)
                     surroundedWithColor:[Util getTransparentColor]
                              withRadius:40];
        [[self layer] addSublayer:_profilePicture];
        
        // fastProfile button
        _buttonFastProfile = [UIButton buttonWithType:UIButtonTypeCustom];
        _buttonFastProfile.frame = CGRectMake(positionX-10, 5, 80, 80);
        _buttonFastProfile.tag = 0;
        _buttonFastProfile.backgroundColor = [UIColor clearColor];
        _buttonFastProfile.layer.cornerRadius = 40; // this value vary as per your desire
        _buttonFastProfile.clipsToBounds = YES;
        _buttonFastProfile.font = [UIFont fontWithName:@"Helvetica Neue Light" size:(12)];
        [_buttonFastProfile setTitle:@"10:00" forState:UIControlStateNormal];
        [_buttonFastProfile setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
        [self addSubview:_buttonFastProfile];
        
        [_buttonFastProfile addTarget:self action:@selector(fastProfile:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
}

-(IBAction) fastProfile :(id)sender{
    if(_viewExpanded == false){
        _viewExpanded = true;
        if( _buttonFastProfile.tag == 1 ){
            _background.image = [UIImage imageNamed:@"fastProfileRight.png"];
            [UIView animateWithDuration:1.0f // This can be changed.
                             animations:^{
                                 CGRect background = _background.frame;
                                 background.size.height += 50.0f;
                                 background.size.width = 320.0f;
                                 background.origin.y -= 25;
                                 background.origin.x = 0;
                                 _background.frame = background;
                                 //userNameLabel
                                 CGRect username = _userNameLabel.frame;
                                 username.origin.x -= 70.0f;
                                 username.origin.y += 6.0f;
                                 _userNameLabel.frame = username;
                                 //winkButton
                                 CGRect winkButton = _winkButton.frame;
                                 winkButton.origin.x -= 125.0f;
                                 _winkButton.frame = winkButton;
                                 _ageNameLabel = [ [UILabel alloc ] initWithFrame:CGRectMake(_userNameLabel.frame.origin.x, _userNameLabel.frame.origin.y+25, 170, 42)];
                                 _ageNameLabel.textAlignment =  UITextAlignmentRight;
                                 _ageNameLabel.textColor = [UIColor clearColor];
                                 _ageNameLabel.backgroundColor = [UIColor clearColor];
                                 _ageNameLabel.font = [UIFont fontWithName:@"Helvetica Neue Light" size:(13)];
                                 _ageNameLabel.text = [[NSString stringWithFormat:@"%d",_userBox._age ] stringByAppendingString:@" ans"];
                                 [self addSubview:_ageNameLabel];
                                 // On crée le Label
                                 _cityNameLabel = [ [UILabel alloc ] initWithFrame:CGRectMake(_userNameLabel.frame.origin.x, _userNameLabel.frame.origin.y+50, 170, 42)];
                                 _cityNameLabel.textAlignment =  UITextAlignmentRight;
                                 _cityNameLabel.textColor = [UIColor clearColor];
                                 _cityNameLabel.backgroundColor = [UIColor clearColor];
                                 _cityNameLabel.font = [UIFont fontWithName:@"Helvetica Neue Light" size:(13)];
                                 _cityNameLabel.text = _userBox._city;
                                 [self addSubview:_cityNameLabel];
                                 
                             }
                             completion:^(BOOL finished){
                                 [UIView animateWithDuration:1.0f // This can be changed.
                                                  animations:^{
                                          _ageNameLabel.textColor = [UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:1.0f];
                                          _cityNameLabel.textColor = [UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:1.0f];
                                                  }];
                             }
             ];

        }else{
            _background.image = [UIImage imageNamed:@"fastProfileLeft.png"];
            [UIView animateWithDuration:1.0f // This can be changed.
                             animations:^{
                                 CGRect background = _background.frame;
                                 background.size.height += 50.0f;
                                 background.size.width = 320.0f;
                                 background.origin.y -= 25;
                                 background.origin.x = 0;
                                 _background.frame = background;
                                 //userNameLabel
                                 CGRect username = _userNameLabel.frame;
                                 username.origin.x += 70.0f;
                                 username.origin.y += 6.0f;
                                 _userNameLabel.frame = username;
                                 //winkButton
                                 CGRect winkButton = _winkButton.frame;
                                 winkButton.origin.x += 120.0f;
                                 _winkButton.frame = winkButton;
                                 _ageNameLabel = [ [UILabel alloc ] initWithFrame:CGRectMake(_userNameLabel.frame.origin.x, _userNameLabel.frame.origin.y+25, 170, 42)];
                                 _ageNameLabel.textAlignment =  UITextAlignmentLeft;
                                 _ageNameLabel.textColor = [UIColor clearColor];
                                 _ageNameLabel.backgroundColor = [UIColor clearColor];
                                 _ageNameLabel.font = [UIFont fontWithName:@"Helvetica Neue Light" size:(13)];
                                 _ageNameLabel.text = [[NSString stringWithFormat:@"%d",_userBox._age ] stringByAppendingString:@" ans"];
                                 [self addSubview:_ageNameLabel];
                                 // On crée le Label
                                 _cityNameLabel = [ [UILabel alloc ] initWithFrame:CGRectMake(_userNameLabel.frame.origin.x, _userNameLabel.frame.origin.y+50, 170, 42)];
                                 _cityNameLabel.textAlignment =  UITextAlignmentLeft;
                                 _cityNameLabel.textColor = [UIColor clearColor];
                                 _cityNameLabel.backgroundColor = [UIColor clearColor];
                                 _cityNameLabel.font = [UIFont fontWithName:@"Helvetica Neue Light" size:(13)];
                                 _cityNameLabel.text = _userBox._city;
                                 [self addSubview:_cityNameLabel];
                                 
                             }
                             completion:^(BOOL finished){
                                 
                                 [UIView animateWithDuration:0.3f // This can be changed.
                                                  animations:^{
                                 _ageNameLabel.textColor = [UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:1.0f];
                                 _cityNameLabel.textColor = [UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:1.0f];
                                                  }];
                             }
             ];

        }
    }else{
        _viewExpanded = false;
        if( _buttonFastProfile.tag == 1 ){
            [UIView animateWithDuration:1.0f // This can be changed.
                             animations:^{
                                 CGRect background = _background.frame;
                                 background.size.height -= 50.0f;
                                 background.size.width = 170.0f;
                                 background.origin.y += 25;
                                 background.origin.x = _defaultpositionX-10;
                                 _background.frame = background;
                                 //userNameLabel
                                 CGRect username = _userNameLabel.frame;
                                 username.origin.x += 70.0f;
                                 username.origin.y -= 6.0f;
                                 _userNameLabel.frame = username;
                                 //winkButton
                                 CGRect winkButton = _winkButton.frame;
                                 winkButton.origin.x += 125.0f;
                                 _winkButton.frame = winkButton;
                                 [_ageNameLabel removeFromSuperview];
                                 [_cityNameLabel removeFromSuperview]; 
                             }];
            _background.image = [UIImage imageNamed:@"personneDroite.png"];
        }else{
            [UIView animateWithDuration:1.0f // This can be changed.
                             animations:^{
                                 CGRect background = _background.frame;
                                 background.size.height -= 50.0f;
                                 background.size.width = 170.0f;
                                 background.origin.y += 25;
                                 background.origin.x = _defaultpositionX+10;
                                 _background.frame = background;
                                 //userNameLabel
                                 CGRect username = _userNameLabel.frame;
                                 username.origin.x -= 70.0f;
                                 username.origin.y -= 6.0f;
                                 _userNameLabel.frame = username;
                                 //winkButton
                                 CGRect winkButton = _winkButton.frame;
                                 winkButton.origin.x -= 120.0f;
                                 _winkButton.frame = winkButton;
                                 [_ageNameLabel removeFromSuperview];
                                 [_cityNameLabel removeFromSuperview];
                             }];
            _background.image = [UIImage imageNamed:@"personneGauche.png"];
        }
    }
}

-(IBAction) winkPerson :(id)sender {
    if(_winkSend == false){
        _winkSend = true;
        UIButton *clicked = (UIButton *) sender;
        // Affichage des utilisateurs autour de soi
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate.connectedUser getUsersAround];
        User* profil = appDelegate.connectedUser;
        
        NSMutableArray *arrayUsers = appDelegate.connectedUser._usersAround;
        //NSLog(@"user : %@", [[arrayUsers objectAtIndex:clicked.tag] _idFacebook] );
        _couplePeek = [PFObject objectWithClassName:@"Wink"];
        [_couplePeek setObject: profil._idFacebook forKey:@"idUserSender"];
        [_couplePeek setObject:[[arrayUsers objectAtIndex:clicked.tag] _idFacebook] forKey:@"idUserReceiver"];
        [_couplePeek setObject:[NSNumber numberWithBool:NO] forKey:@"accepted"];
        [_couplePeek setObject:[NSNumber numberWithBool:NO] forKey:@"isFinished"];
        [_couplePeek setObject:[PFGeoPoint geoPointWithLocation:[[arrayUsers objectAtIndex:clicked.tag] _location]] forKey:@"position"];
        [_couplePeek saveInBackground];
        [UIView animateWithDuration:0.5f // This can be changed.
         animations:^{
             _buttonFastProfile.backgroundColor = [UIColor grayColor];
             _buttonFastProfile.alpha = 0.8f;
             [_buttonFastProfile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
             
         }];
        _counter = 600;
        
        [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(advanceTimer:)
                                       userInfo:nil repeats:YES];
        
        //Envoie de la notif
        PFQuery *pushQuery = [PFInstallation query];
        [pushQuery whereKey:@"userID" equalTo:self.userBox._idFacebook];
        [PFPush sendPushMessageToQueryInBackground:pushQuery
                                       withMessage:[NSString stringWithFormat:@"%@ %@", profil._firstName, @" vient de t'envoyer un clin d'oeil, réponds-lui vite !"]];
    }
}

- (void)advanceTimer:(NSTimer *)timer
{
    int  minutes = _counter / 60;
    int seconds = _counter % 60;
    [self setCounter:(_counter -1)];
    [_buttonFastProfile setTitle:[NSString stringWithFormat:@"%d:%@", minutes, [NSString stringWithFormat:@"%02d", seconds]] forState:UIControlStateNormal];
    if (_counter <= -2) {
        [timer invalidate];
        [_couplePeek setObject:[NSNumber numberWithBool:YES] forKey:@"isFinished"];
        [_couplePeek saveInBackground];
        [_buttonFastProfile setTitle:@"Terminé" forState:UIControlStateNormal];
    }
}


@end
