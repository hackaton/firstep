//
//  TabOneViewController.m
//  firststep
//
//  Created by Stéphane Bruckert on 7/25/13.
//
//

#import "TabOneViewController.h"
#import "User.h"
// Laisser deux espaces après le mot
#define ARY @[@"Mark Wu  ", @"Green Chiu  ", @"Eikiy Chang  ", @"Gina Sun  ", @"Jeremy Chang  ", @"Sandra Hsu  "]


@implementation TabOneViewController {
}

@synthesize _displayedUser;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Profil";
    }
    return self;
}

- (void)loadView {
    self.tagNames = [NSMutableArray arrayWithArray:ARY];
    [super loadView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _bio.text = _displayedUser._bio;
    _age.text = [NSString stringWithFormat:@"%d", _displayedUser._age];
    if([_displayedUser._gender isEqual: @"male"]){
        _gender.text = @"Homme";
        _titleIntresedBy.text = @"Inréressé par";
    }else{
        _gender.text = @"Femme";
        _titleIntresedBy.text = @"Inréressée par";
    }
    _interesedBy.text = _displayedUser._interestedBy;
    
    // Tags
    self.nibTagList.labelFont = [UIFont systemFontOfSize:12.f];
    [self.nibTagList reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfTagLabelInTagList:(GCTagList *)tagList {
    return self.tagNames.count;
}

- (GCTagLabel*)tagList:(GCTagList *)tagList tagLabelAtIndex:(NSInteger)index {
    
    static NSString* identifier = @"TagLabelIdentifier";
    
    GCTagLabel* tag = [tagList dequeueReusableTagLabelWithIdentifier:identifier];
    if(!tag) {
        tag = [GCTagLabel tagLabelWithReuseIdentifier:identifier];
        
        tag.backgroundColor = [UIColor colorWithRed:177/255.0f green:213/255.0f blue:253/255.0f alpha:1.0f];
        
        [tag setCornerRadius:0.f];
    }
    
    NSString* labelText = self.tagNames[index];
    
    /**
     * you can change the AccrssoryType with method setLabelText:accessoryType:
     * or with no accessoryButton with method setLabelText:
     */
    
    /* way 1 */
    GCTagLabelAccessoryType type = GCTagLabelAccessoryCrossSign;
    [tag setLabelText:labelText
        accessoryType:type];
    
    
    //way 2
    //[tag setLabelText:labelText];
    
    return tag;
}

- (void)tagList:(GCTagList *)tagList accessoryButtonTappedAtIndex:(NSInteger)index {
    
    /**
     * this is the delete method how to use.
     */
    [self.tagNames removeObjectsInRange:NSMakeRange(index, 1)];
    [tagList deleteTagLabelWithRange:NSMakeRange(index, 1)];
    [tagList deleteTagLabelWithRange:NSMakeRange(index, 1) withAnimation:YES];
    
    
    
    /**
     * this is the reload method how to use.
     */
    /**
     self.tagNames[index] = @"Kim Jong Kook";
     [tagList reloadTagLabelWithRange:NSMakeRange(index, 1)];
     [tagList reloadTagLabelWithRange:NSMakeRange(index, 1) withAnimation:YES];
     
     self.tagNames[index] = @"Kim Jong Kook";
     self.tagNames[index+1] = @"Girls' Generation";
     [tagList reloadTagLabelWithRange:NSMakeRange(index, 2) withAnimation:YES];
     */
    
    /**
     * this is the insert method how to use.
     */
    //    [self.tagNames insertObject:@"Girls' Generation" atIndex:index];
    //    [self.tagNames insertObject:@"TaeTiSeo" atIndex:index];
    //    [tagList insertTagLabelWithRange:NSMakeRange(index, 2) withAnimation:YES];
    
}

- (void)tagList:(GCTagList *)taglist didChangedHeight:(CGFloat)newHeight {
    NSLog(@"%s:%.1f", __func__, newHeight);
}

- (NSString*)tagList:(GCTagList *)tagList labelTextForGroupTagLabel:(NSInteger)interruptIndex {
    return [NSString stringWithFormat:@"和其他%d位", self.tagNames.count - interruptIndex];
}

- (void)tagList:(GCTagList *)taglist didSelectedLabelAtIndex:(NSInteger)index {
    [taglist deselectedLabelAtIndex:index animated:YES];
}

/**
 *
 */
//- (NSInteger)maxNumberOfRowAtTagList:(GCTagList *)tagList {
//    return 1;
//}

- (GCTagLabelAccessoryType)accessoryTypeForGroupTagLabel {
    return GCTagLabelAccessoryArrowSign;
}


- (IBAction)modification1:(id)sender {
}

- (IBAction)modification2:(id)sender {
}
@end
