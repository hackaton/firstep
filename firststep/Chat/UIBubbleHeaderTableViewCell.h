//
//  UIBubbleHeaderTableViewCell.h
//  firststep
//
//  Created by Morgan BRASSEUR on 22/05/13.
//
//

#import <UIKit/UIKit.h>

@interface UIBubbleHeaderTableViewCell : UITableViewCell

+ (CGFloat)height;

@property (nonatomic, strong) NSDate* date;

@end
