//
//  UIBubbleTableView.h
//  firststep
//
//  Created by Morgan BRASSEUR on 22/05/13.
//
//

#import <UIKit/UIKit.h>

#import "UIBubbleTableViewDataSource.h"
#import "UIBubbleTableViewCell.h"

typedef enum BubbleTypingState
{
    TYPING_NOBODY       = 0,
    TYPING_ME           = 1,
    TYPING_SOMEBODY     = 2
} NSBubbleTypingState;

@interface UIBubbleTableView : UITableView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) NSTimeInterval sectionTimeInterval;
@property (nonatomic) BOOL showAvatars;
@property (nonatomic) NSBubbleTypingState typingState;
@property (nonatomic, assign) IBOutlet id<UIBubbleTableViewDataSource> bubbleTableDataSource;

@end
