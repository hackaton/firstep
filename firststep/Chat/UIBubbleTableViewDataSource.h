//
//  UIBubbleTableViewDataSource.h
//  firststep
//
//  Created by Morgan BRASSEUR on 22/05/13.
//
//

#import <Foundation/Foundation.h>

@class NSBubbleData;
@class UIBubbleTableView;

@protocol UIBubbleTableViewDataSource <NSObject> 

@optional

@required // You need to implement those methodes

- (NSInteger)rowsForBubbleTable:(UIBubbleTableView*)tableView;
- (NSBubbleData*)bubbleTableView:(UIBubbleTableView*)tableView dataForRow:(NSInteger)row;

@end
