//
//  Message.m
//  firststep
//
//  Created by Morgan BRASSEUR on 22/05/13.
//
//

#import "Message.h"
	
@implementation Message

@synthesize _content;
@synthesize _userSender;
@synthesize _userReceiver;

- (id) initWithAll              : (NSString*)content
                   userSender   : (NSString*)userSender
                   userReceiver : (NSString*)userReceiver
                   date         : (NSDate*)date
{
    self = [super init];
    if (!self) return nil;
    
    self._content = content;
    self._userSender = userSender;
    self._userReceiver = userReceiver;
    self._date = date;
    return self;
}

- (id) initFromDatabase         :(PFObject*)databaseMessage
{
    self = [super init];
    if (!self) return nil;
    
    self._content = [databaseMessage objectForKey : @"content"];
    self._userSender = [databaseMessage objectForKey : @"idUsersSender"];
    self._userReceiver = [databaseMessage objectForKey : @"idUsersReceiver"];
    self._date = [databaseMessage updatedAt];
    
    return self;
}

- (void) save
{
    PFObject* databaseMessage = [PFObject objectWithClassName:@"Messages"];
    [databaseMessage setObject:_content forKey:@"content"];
    [databaseMessage setObject:_userSender forKey:@"idUsersSender"];
    [databaseMessage setObject:_userReceiver forKey:@"idUsersReceiver"];
    [databaseMessage saveInBackground];
}

- (void) update
{
    PFObject* databaseMessage = [PFObject objectWithClassName:@"Messages"];
    [databaseMessage setObject:_content forKey:@"content"];
    [databaseMessage setObject:_userSender forKey:@"idUsersSender"];
    [databaseMessage setObject:_userReceiver forKey:@"idUsersReceiver"];
    [databaseMessage saveInBackground];
}

+ (NSMutableArray*) getNewMessages      : (NSDate*) lastDate
                        userSender      : (NSString*) userSender
                        userReceiver    : (NSString*) userReceiver
{
    
    PFQuery* query = [PFQuery queryWithClassName:@"Messages"];
    [query whereKey:@"idUsersSender" equalTo:userSender];
    [query whereKey:@"idUsersReceiver" equalTo:userReceiver];
    [query whereKey:@"updatedAt" greaterThan:lastDate];
    [query orderByAscending:@"updatedAt"];
    
    NSArray* arrayValue = [query findObjects];
    
    NSMutableArray* newMessages = [[NSMutableArray alloc] initWithCapacity:arrayValue.count];
    
    for(int i=0; i<arrayValue.count; i++)
    {
        Message* message = [[Message alloc] initFromDatabase:(PFObject*)[arrayValue objectAtIndex:i]];
        [newMessages addObject:message];
    }
    
    return newMessages;
}

+ (NSMutableArray*) getAllMessages      : (NSString*) userSender
                        userReceiver    : (NSString*) userReceiver
                        limit           : (int)limit
{    
    PFQuery* query = [PFQuery queryWithClassName:@"Messages"];
    [query whereKey:@"idUsersSender" containedIn:@[userSender, userReceiver]];
    [query whereKey:@"idUsersReceiver" containedIn:@[userReceiver, userSender]];
    [query setLimit:limit];
    [query orderByAscending:@"updatedAt"];
    
    NSArray* arrayValue = [query findObjects];
    
    NSMutableArray* newMessages = [[NSMutableArray alloc] initWithCapacity:arrayValue.count];
    
    for(int i=0; i<arrayValue.count; i++)
    {
        Message* message = [[Message alloc] initFromDatabase:(PFObject*)[arrayValue objectAtIndex:i]];
        [newMessages addObject:message];
    }
    
    return newMessages;
}

@end

