//
//  Message.h
//  firststep
//
//  Created by Morgan BRASSEUR on 22/05/13.
//
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface Message : NSObject

// ATTRIBUTS
@property (nonatomic, retain) NSString* _content;
@property (nonatomic, retain) NSString* _userSender;
@property (nonatomic, retain) NSString* _userReceiver;
@property (nonatomic, retain) NSDate* _date;

// METHODS
- (id) initWithAll                      : (NSString*) content
                        userSender      : (NSString*) userSender
                        userReceiver    : (NSString*) userReceiver
                        date            : (NSDate*) date;

- (id) initFromDatabase                 : (PFObject*) databaseMessage;

- (void) save;
- (void) update;
+ (NSMutableArray*) getNewMessages      : (NSDate*) lastDate
                        userSender      : (NSString*) userSender
                        userReceiver    : (NSString*) userReceiver;
+ (NSMutableArray*) getAllMessages      : (NSString*) userSender
                        userReceiver    : (NSString*) userReceiver
                        limit           : (int) limit;

@end
