//
//  UIBubbleTableView.m
//  firststep
//
//  Created by Morgan BRASSEUR on 22/05/13.
//
//

#import "UIBubbleTableView.h"
#import "NSBubbleData.h"
#import "UIBubbleHeaderTableViewCell.h"
#import "UIBubbleTypingTableViewCell.h"

@interface UIBubbleTableView ()

@property (nonatomic, retain) NSMutableArray* bubbleTableSection;

@end

@implementation UIBubbleTableView

@synthesize bubbleTableDataSource = _bubbleTableDataSource;
@synthesize sectionTimeInterval = _sectionTimeInterval;
@synthesize bubbleTableSection = _bubbleTableSection;
@synthesize typingState = _typingState;
@synthesize showAvatars = _showAvatars;

const int AVATAR_HEIGHT = 52;

- (void)initializator
{
    // UITableView properties
    
    self.backgroundColor = [UIColor clearColor];
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    assert(self.style == UITableViewStylePlain);
    
    self.delegate = self;
    self.dataSource = self;
    
    // Specific properties
    
    self.sectionTimeInterval = 600;
    self.typingState = TYPING_NOBODY;
    self.showAvatars = FALSE;
}

- (id)init
{
    self = [super init];
    if (self) [self initializator];
    return self;
}

- (id)initWithCoder:(NSCoder*)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:UITableViewStylePlain];
    if (self) [self initializator];
    return self;
}

- (void)reloadData
{
    self.showsVerticalScrollIndicator = YES;
    self.showsHorizontalScrollIndicator = NO;
    
	self.bubbleTableSection = nil;
    
    int count = 0;
    self.bubbleTableSection = [[NSMutableArray alloc] init];
    
    if(self.bubbleTableDataSource && (count = [self.bubbleTableDataSource rowsForBubbleTable:self]) > 0)
    {
        NSMutableArray* bubbleData = [[NSMutableArray alloc] initWithCapacity:count];
        
        for(int i=0; i<count; i++)
        {
            NSObject* object = [self.bubbleTableDataSource bubbleTableView:self dataForRow:i];
            assert([object isKindOfClass:[NSBubbleData class]]);
            [bubbleData addObject:object];
        }
        
        // Sort by date
        [bubbleData sortUsingComparator:^NSComparisonResult(id obj1, id obj2)
         {
             NSBubbleData* bubbleData1 = (NSBubbleData*)obj1;
             NSBubbleData* bubbleData2 = (NSBubbleData*)obj2;
             
             return [bubbleData1.date compare:bubbleData2.date];            
         }];
        
        NSDate* lastDate = [NSDate dateWithTimeIntervalSince1970:0];
        NSMutableArray* currentSection = nil;
        
        for(int i=0; i<count; i++)
        {
            NSBubbleData* data = (NSBubbleData*)[bubbleData objectAtIndex:i];
            
            if([data.date timeIntervalSinceDate:lastDate] > self.sectionTimeInterval)
            {
                currentSection = [[NSMutableArray alloc] init];
                [self.bubbleTableSection addObject:currentSection];
            }
            
            [currentSection addObject:data];
            lastDate = data.date;
        }
    }
    
    [super reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    int result = [self.bubbleTableSection count];
    if(self.typingState != TYPING_NOBODY)
    {
        result++;
    }
    return result;
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    // If is typing
	if(section >= [self.bubbleTableSection count])
    {
        return 1;   
    }
    return [[self.bubbleTableSection objectAtIndex:section] count] + 1;
}

- (float)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    // If is typing now
	if(indexPath.section >= [self.bubbleTableSection count])
    {
        return MAX([UIBubbleTypingTableViewCell height], self.showAvatars ? AVATAR_HEIGHT : 0);
    }
    
    // if header
    if(indexPath.row == 0)
    {
        return [UIBubbleHeaderTableViewCell height];
    }
    
    NSBubbleData* data = [[self.bubbleTableSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row - 1];
    return MAX(data.insets.top + data.view.frame.size.height + data.insets.bottom, self.showAvatars ? AVATAR_HEIGHT : 0);
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    // If is typing now
	if(indexPath.section >= [self.bubbleTableSection count])
    {
        static NSString* cellId = @"tblBubbleTypingCell";
        UIBubbleTypingTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if(cell == nil)
        {
            cell = [[UIBubbleTypingTableViewCell alloc] init];
        }

        cell.typingState = self.typingState;
        cell.showAvatar = self.showAvatars;
        
        return cell;
    }

    // Header with date and time
    if(indexPath.row == 0)
    {
        static NSString* cellId = @"tblBubbleHeaderCell";
        UIBubbleHeaderTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        NSBubbleData* data = [[self.bubbleTableSection objectAtIndex:indexPath.section] objectAtIndex:0];
        
        if(cell == nil)
        {
            cell = [[UIBubbleHeaderTableViewCell alloc] init];
        }
        cell.date = data.date;
       
        return cell;
    }
    
    // Standard bubble    
    static NSString* cellId = @"tblBubbleCell";
    UIBubbleTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    NSBubbleData* data = [[self.bubbleTableSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row - 1];
    
    if(cell == nil)
    {
        cell = [[UIBubbleTableViewCell alloc] init];
    }
    cell.data = data;
    cell.showAvatar = self.showAvatars;
    
    return cell;
}

@end
