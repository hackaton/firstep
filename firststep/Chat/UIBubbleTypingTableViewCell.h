//
//  UIBubbleTypingTableViewCell.h
//  firststep
//
//  Created by Morgan BRASSEUR on 22/05/13.
//
//

#import <UIKit/UIKit.h>
#import "UIBubbleTableView.h"


@interface UIBubbleTypingTableViewCell : UITableViewCell

+ (CGFloat)height;

@property (nonatomic) NSBubbleTypingState typingState;
@property (nonatomic) BOOL showAvatar;

@end
