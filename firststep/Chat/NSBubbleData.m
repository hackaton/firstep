//
//  NSBubbleData.m
//  firststep
//
//  Created by Morgan BRASSEUR on 22/05/13.
//
//

#import "NSBubbleData.h"
#import <QuartzCore/QuartzCore.h>

@implementation NSBubbleData

@synthesize origin = _origin;
@synthesize date = _date;
@synthesize avatar = _avatar;
@synthesize view = _view;
@synthesize insets = _insets;

const UIEdgeInsets INSETS_TEXT_FROM_ME         = {4, 11, 10, 21};
const UIEdgeInsets INSETS_TEXT_FROM_SOMEBODY   = {4, 18, 10, 14};

const UIEdgeInsets INSETS_IMAGE_FROM_ME        = {11, 11, 16, 21};
const UIEdgeInsets INSETS_IMAGE_FROM_SOMEBODY  = {11, 18, 16, 14};

const int BUBBLE_WIDTH = 220;

- (id)initWithText:(NSString*)text date:(NSDate*)date origin:(NSBubbleOrigin)origin
{
    UIFont* font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    CGSize size = [(text ? text : @"") sizeWithFont:font constrainedToSize:CGSizeMake(BUBBLE_WIDTH, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.text = (text ? text : @"");
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets insets = (origin == BUBBLE_FROM_ME ? INSETS_TEXT_FROM_ME : INSETS_TEXT_FROM_SOMEBODY);
    return [self initWithView:label date:date origin:origin insets:insets];
}

- (id)initWithImage:(UIImage*)image date:(NSDate*)date origin:(NSBubbleOrigin)origin
{
    CGSize size = image.size;
    if (size.width > BUBBLE_WIDTH)
    {
        size.height /= (size.width / BUBBLE_WIDTH);
        size.width = BUBBLE_WIDTH;
    }
    
    UIImageView* bubbleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    bubbleImageView.image = image;
    bubbleImageView.layer.cornerRadius = 5.0;
    bubbleImageView.layer.masksToBounds = YES;
    
    UIEdgeInsets insets = (origin == BUBBLE_FROM_ME ? INSETS_IMAGE_FROM_ME : INSETS_IMAGE_FROM_SOMEBODY);
    return [self initWithView:bubbleImageView date:date origin:origin insets:insets];
}

- (id)initWithView:(UIView*)view date:(NSDate*)date origin:(NSBubbleOrigin)origin insets:(UIEdgeInsets)insets
{
    self = [super init];
    if (self)
    {
        _view = view;
        _date = date;
        _origin = origin;
        _insets = insets;
    }
    return self;
}

+ (id)dataWithText:(NSString*)text date:(NSDate*)date origin:(NSBubbleOrigin)origin
{
    return [[NSBubbleData alloc] initWithText:text date:date origin:origin];
}

+ (id)dataWithImage:(UIImage*)image date:(NSDate*)date origin:(NSBubbleOrigin)origin
{
    return [[NSBubbleData alloc] initWithImage:image date:date origin:origin];
}

+ (id)dataWithView:(UIView*)view date:(NSDate*)date origin:(NSBubbleOrigin)origin insets:
    (UIEdgeInsets)insets
{
    return [[NSBubbleData alloc] initWithView:view date:date origin:origin insets:insets];
}

@end
