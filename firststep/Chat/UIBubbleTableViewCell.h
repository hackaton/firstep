//
//  UIBubbleTableViewCell.h
//  firststep
//
//  Created by Morgan BRASSEUR on 22/05/13.
//
//

#import <UIKit/UIKit.h>
#import "NSBubbleData.h"

@interface UIBubbleTableViewCell : UITableViewCell

@property (nonatomic, strong) NSBubbleData* data;
@property (nonatomic) BOOL showAvatar;

@end
