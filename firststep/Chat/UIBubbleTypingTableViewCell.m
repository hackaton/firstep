//
//  UIBubbleTypingTableViewCell.m
//  firststep
//
//  Created by Morgan BRASSEUR on 22/05/13.
//
//

#import "UIBubbleTypingTableViewCell.h"

@interface UIBubbleTypingTableViewCell ()

@property (nonatomic, retain) UIImageView* typingImageView;

@end

@implementation UIBubbleTypingTableViewCell

@synthesize typingState = _typingState;
@synthesize typingImageView = _typingImageView;
@synthesize showAvatar = _showAvatar;

+ (CGFloat)height
{
    return 40.0;
}

- (void)setTypingState:(NSBubbleTypingState)value
{
    if (!self.typingImageView)
    {
        self.typingImageView = [[UIImageView alloc] init];
        [self addSubview:self.typingImageView];
    }
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImage *bubbleImage = nil;
    CGFloat x = 0;
    
    if (value == TYPING_ME)
    {
        bubbleImage = [UIImage imageNamed:@"typingMe.png"]; 
        x = self.frame.size.width - bubbleImage.size.width;
    }
    else
    {
        bubbleImage = [UIImage imageNamed:@"typingSomebody.png"]; 
        x = 0;
    }
    
    self.typingImageView.image = bubbleImage;
    self.typingImageView.frame = CGRectMake(x, 4, 73, 31);
}

@end
