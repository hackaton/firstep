//
//  NSBubbleData.h
//  firststep
//
//  Created by Morgan BRASSEUR on 22/05/13.
//
//

#import <Foundation/Foundation.h>

typedef enum BubbleOrigin
{
    BUBBLE_FROM_ME          = 0,
    BUBBLE_FROM_SOMEBODY    = 1
} NSBubbleOrigin;

@interface NSBubbleData : NSObject

@property (readonly, nonatomic) NSBubbleOrigin origin;
@property (readonly, nonatomic, strong) NSDate* date;
@property (nonatomic, strong) UIImage* avatar;
@property (readonly, nonatomic, strong) UIView* view;
@property (readonly, nonatomic) UIEdgeInsets insets;

- (id)initWithText:(NSString*)text date:(NSDate*)date origin:(NSBubbleOrigin)origin;
- (id)initWithImage:(UIImage*)image date:(NSDate*)date origin:(NSBubbleOrigin)origin;
- (id)initWithView:(UIView*)view date:(NSDate*)date origin:(NSBubbleOrigin)origin insets:(UIEdgeInsets)insets;

+ (id)dataWithText:(NSString*)text date:(NSDate*)date origin:(NSBubbleOrigin)origin;
+ (id)dataWithImage:(UIImage*)image date:(NSDate*)date origin:(NSBubbleOrigin)origin;
+ (id)dataWithView:(UIView*)view date:(NSDate*)date origin:(NSBubbleOrigin)origin insets:(UIEdgeInsets)insets;

@end
