//
//  TabOneViewController.h
//  firststep
//
//  Created by Stéphane Bruckert on 7/25/13.
//
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "User.h"

@interface TabSecViewController : UIViewController {
    User * _displayedUser;
}

@property (nonatomic, retain) User * _displayedUser;

@end
