//
//  TabOneViewController.h
//  firststep
//
//  Created by Stéphane Bruckert on 7/25/13.
//
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "User.h"
#import "GCTagList.h"

@interface TabOneViewController : UIViewController <GCTagListDataSource, GCTagListDelegate> {
    User * _displayedUser;
}

@property (nonatomic, retain) NSMutableArray* tagNames;

@property (nonatomic, GC_STRONG) IBOutlet GCTagList *nibTagList;

@property (nonatomic, retain) User * _displayedUser;

@property (nonatomic, weak) IBOutlet UILabel* bio;

@property (nonatomic, weak) IBOutlet UILabel* age;

@property (weak, nonatomic) IBOutlet UILabel *gender;

@property (weak, nonatomic) IBOutlet UILabel *titleIntresedBy;

@property (weak, nonatomic) IBOutlet UILabel *interesedBy;

@property (weak, nonatomic) IBOutlet UIButton *buttonModification1;
- (IBAction)modification1:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *buttonModification2;
- (IBAction)modification2:(id)sender;

@end
