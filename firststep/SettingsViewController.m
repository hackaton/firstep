//
//  SettingsViewController.m
//  firststep
//
//  Created by Donovan Charpin on 16/05/13.
//
//

#import "AppDelegate.h"
#import "SettingsViewController.h"



@interface SettingsViewController ()

@property (strong, nonatomic) FBUserSettingsViewController *settingsViewController;

@end

@implementation SettingsViewController

@synthesize selectedIndex;

@synthesize ibSwitch;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ibSwitch.onColor = [UIColor colorWithRed:0.20f green:0.42f blue:0.86f alpha:1.00f];
    ibSwitch.on = YES;
    
    
    SevenSwitch *switchGeolocalisation = [[SevenSwitch alloc] initWithFrame:CGRectZero];
    switchGeolocalisation.center = CGPointMake(280,122);
    [switchGeolocalisation addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:switchGeolocalisation];
    [switchGeolocalisation setOn:YES animated:YES];

    SevenSwitch *switchNotification = [[SevenSwitch alloc] initWithFrame:CGRectZero];
    switchNotification.center = CGPointMake(280,204);
    [switchNotification addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:switchNotification];
    [switchNotification setOn:YES animated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil fbUser:(FBUserSettingsViewController *)settingsViewController
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _settingsViewController = settingsViewController;
    }
    return self;
}

- (IBAction)logoutFb:(id)sender {
    [FBSession.activeSession closeAndClearTokenInformation];
    [FBSession.activeSession close];
}

- (IBAction)backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)switchChanged:(SevenSwitch *)sender {
    NSLog(@"Changed value to: %@", sender.on ? @"ON" : @"OFF");
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    
    self.navigationController.navigationBarHidden = YES; //suppression barre
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillDisappear:animated];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


@end
