//
//  ChatViewController.h
//  firststep
//
//  Created by Donovan Charpin on 15/05/13.
//
//

#import <UIKit/UIKit.h>
#import "UIBubbleTableViewDataSource.h"

@interface ChatViewController : UIViewController <UIBubbleTableViewDataSource>

// ATTRIBUTS
@property (nonatomic, retain) NSDate* _lastDate;
@property (nonatomic, retain) NSTimer* _timer;

//METHODES
- (IBAction)returnToFirstView:(id)sender;
- (IBAction)backButton:(id)sender;

@end
