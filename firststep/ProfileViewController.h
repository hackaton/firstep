//
//  ProfileViewController.h
//  firststep
//
//  Created by Stéphane Bruckert on 7/2/13.
//
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "SegmentsController.h"
#import "SDSegmentedControl.h"

@interface ProfileViewController : UIViewController {
    
    IBOutlet UIView *tabView;
    IBOutlet UIScrollView *scroller;
    User* _displayedUser;
    SegmentsController *segmentsController;
    UISegmentedControl *segmentedControl;

}

@property (nonatomic, weak) IBOutlet UIImageView* profileBgImageView;

@property (nonatomic, weak) IBOutlet UILabel* name;

@property (nonatomic, weak) IBOutlet UILabel* city;

@property (nonatomic, weak) IBOutlet UIView* overlayView;

@property (nonatomic, weak) IBOutlet UIView* tabView;

@property (nonatomic) IBOutlet SegmentsController *segmentsController;

@property (nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (weak, nonatomic) IBOutlet UILabel *selectedSegmentLabel;

@property (nonatomic) UIView *modeMoficiationRectangle;
@property (nonatomic) UILabel *modeMoficiationlabel;

@property (nonatomic, assign) BOOL modeModificationAtivate;

- (IBAction)segmentDidChange:(id)sender;

- (IBAction)backButton:(id)sender;

- (id)initWithUser:(User *)user;

@property (weak, nonatomic) IBOutlet UIButton *buttonModification;

- (IBAction)modificationMode:(id)sender;

@end
