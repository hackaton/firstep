//
//  SettingsViewController.h
//  firststep
//
//  Created by Donovan Charpin on 16/05/13.
//
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "SevenSwitch.h"

@interface SettingsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *selectedIndex;

@property (nonatomic, strong) IBOutlet SevenSwitch *ibSwitch;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil fbUser:(FBUserSettingsViewController *)settingsViewController;

- (IBAction)logoutFb:(id)sender;

- (IBAction)backButton:(id)sender;

@end
