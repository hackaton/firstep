//
//  RMSwipeTableViewController.m
//  RMSwipeTableView
//
//  Created by Rune Madsen on 2012-11-24.
//  Copyright (c) 2012 The App Boutique. All rights reserved.
//

#import "RMSwipeTableView.h"
#import "RMSwipeTableViewCell.h"
#import "RMPersonTableViewCell.h"
#import "ChatViewController.h"

#define LOG_DELEGATE_METHODS 0

@interface RMSwipeTableView ()

@end

@implementation RMSwipeTableView

@synthesize swipeTableDataSource = _swipeTableDataSource;

- (void)initializator
{
    // UITableView properties
    
    self.backgroundColor = [UIColor clearColor];
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    assert(self.style == UITableViewStylePlain);
    self.delegate = self;
    self.dataSource = self;
        
    static NSString *CellIdentifier = @"Cell";
    [self registerClass:[RMPersonTableViewCell class] forCellReuseIdentifier:CellIdentifier];
    [self setRowHeight:64];
     
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), 200)];
    [view setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [view setBackgroundColor:[UIColor colorWithWhite:0.92 alpha:1]];
    UIImageView *sigilImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"HouseStarkSigil"]];
    [sigilImageView setCenter:view.center];
    [sigilImageView setAlpha:0.3];
    [view addSubview:sigilImageView];
    self.tableFooterView = view;
    
    [self setBackgroundColor:view.backgroundColor];
    
    self.contentInset = self.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, - CGRectGetHeight(view.frame), 0);
}

- (id)init {
    if (self) [self initializator];
    return self;
}

- (id)initWithCoder:(NSCoder*)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) [self initializator];
    return self;
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:UITableViewStylePlain];
    if (self) [self initializator];
    return self;
}

-(void)resetTableView {
    [_array removeAllObjects];
    _array = nil;
    [self reloadData];
}

-(NSMutableArray*)array {
    if (!_array) {
        _array = [@[
                  [@{@"name" : @"Cersei Lannister", @"title" : @"Queen of the Seven Kingdoms", @"isFavourite" : @YES, @"image" : @"cersei" } mutableCopy],
                  [@{@"name" : @"Jaime Lannister", @"title" : @"Kingslayer", @"isFavourite" : @NO, @"image" : @"jaime" } mutableCopy],
                  [@{@"name" : @"Joffrey Baratheon", @"title" : @"The Illborn", @"isFavourite" : @NO, @"image" : @"joffrey" } mutableCopy],
                  [@{@"name" : @"Tyrion Lannister", @"title" : @"The Halfman", @"isFavourite" : @NO, @"image" : @"tyrion" } mutableCopy],
                  [@{@"name" : @"Tywin Lannister", @"title" : @"Lord of Casterly Rock", @"isFavourite" : @NO, @"image" : @"tywin" } mutableCopy]]
                  mutableCopy];
    }
    return _array;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.array count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";    
    RMPersonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = [[self.array objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.detailTextLabel.text = [[self.array objectAtIndex:indexPath.row] objectForKey:@"title"];
    [cell setThumbnail:[UIImage imageNamed:[[self.array objectAtIndex:indexPath.row] objectForKey:@"image"]]];
    [cell setFavourite:[[[self.array objectAtIndex:indexPath.row] objectForKey:@"isFavourite"] boolValue] animated:NO];
    //Ceci délègue les 2 mouvements aux cellules!
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

#pragma mark - Swipe Table View Cell Delegate

-(void)swipeTableViewCellDidStartSwiping:(RMSwipeTableViewCell *)swipeTableViewCell {
#if LOG_DELEGATE_METHODS
    NSLog(@"swipeTableViewCellDidStartSwiping: %@", swipeTableViewCell);
#endif
    //Pour la droite
    NSIndexPath *indexPathForCell = [self indexPathForCell:swipeTableViewCell];
    if (self.selectedIndexPath.row != indexPathForCell.row) {
        [self resetSelectedCell];
    }
}

-(void)swipeTableViewCell:(RMPersonTableViewCell *)swipeTableViewCell didSwipeToPoint:(CGPoint)point velocity:(CGPoint)velocity {
#if LOG_DELEGATE_METHODS
    NSLog(@"swipeTableViewCell: %@ didSwipeToPoint: %@ velocity: %@", swipeTableViewCell, NSStringFromCGPoint(point), NSStringFromCGPoint(velocity));
#endif
}

-(void)swipeTableViewCellWillResetState:(RMSwipeTableViewCell *)swipeTableViewCell fromPoint:(CGPoint)point animation:(RMSwipeTableViewCellAnimationType)animation velocity:(CGPoint)velocity {
#if LOG_DELEGATE_METHODS
    NSLog(@"swipeTableViewCellWillResetState: %@ fromPoint: %@ animation: %d, velocity: %@", swipeTableViewCell, NSStringFromCGPoint(point), animation, NSStringFromCGPoint(velocity));
#endif
    if (point.x >= CGRectGetHeight(swipeTableViewCell.frame)) {
        NSIndexPath *indexPath = [self indexPathForCell:swipeTableViewCell];
        if ([[[self.array objectAtIndex:indexPath.row] objectForKey:@"isFavourite"] boolValue]) {
            [[self.array objectAtIndex:indexPath.row] setObject:@NO forKey:@"isFavourite"];
        } else {
            [[self.array objectAtIndex:indexPath.row] setObject:@YES forKey:@"isFavourite"];
        }
        [(RMPersonTableViewCell*)swipeTableViewCell setFavourite:[[[self.array objectAtIndex:indexPath.row] objectForKey:@"isFavourite"] boolValue] animated:YES];
    }
    //Pour la droite
    if (velocity.x <= -500) {
        self.selectedIndexPath = [self indexPathForCell:swipeTableViewCell];
        swipeTableViewCell.shouldAnimateCellReset = NO;
        swipeTableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSTimeInterval duration = MAX(-point.x / ABS(velocity.x), 0.10f);
        [UIView animateWithDuration:duration
                              delay:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             swipeTableViewCell.contentView.frame = CGRectOffset(swipeTableViewCell.contentView.bounds, point.x - (ABS(velocity.x) / 150), 0);
                         }
                         completion:^(BOOL finished) {
                             [UIView animateWithDuration:duration
                                                   delay:0
                                                 options:UIViewAnimationOptionCurveEaseOut
                                              animations:^{
                                                  swipeTableViewCell.contentView.frame = CGRectOffset(swipeTableViewCell.contentView.bounds, -80, 0);
                                              }
                                              completion:^(BOOL finished) {
                                              }];
                         }];
    }
}

//Pour la droite
-(void)resetSelectedCell {
    RMSwipeTableViewCell *cell = (RMSwipeTableViewCell *)[self cellForRowAtIndexPath:self.selectedIndexPath];
    [cell resetContentView];
    self.selectedIndexPath = nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self deselectRowAtIndexPath:indexPath animated:YES];
    [self resetSelectedCell];
    NSLog(@"1");
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (self.selectedIndexPath) {
        [self resetSelectedCell];
    }
    NSLog(@"2");
}

#pragma mark - RMSwipeTableViewCell delegate method

-(void)swipeTableViewCellDidDelete:(RMSwipeTableViewCell *)swipeTableViewCell {
    NSIndexPath *indexPath = [self indexPathForCell:swipeTableViewCell];
    [self.array removeObjectAtIndex:indexPath.row];
    [swipeTableViewCell resetContentView];
    [self beginUpdates];
    [self deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self endUpdates];
    NSLog(@"3");
}

@end