//
//  UIBubbleTableViewDataSource.h
//  firststep
//
//  Created by Morgan BRASSEUR on 22/05/13.
//
//

#import <Foundation/Foundation.h>

@class RMBubbleData;
@class RMSwipeTableView;

@protocol RMSwipeTableViewDataSource <NSObject>

@optional

@required // You need to implement those methodes

- (NSInteger)rowsForTable:(RMSwipeTableView*)tableView;
- (RMBubbleData*)tableView:(RMSwipeTableView*)tableView dataForRow:(NSInteger)row;

@end
