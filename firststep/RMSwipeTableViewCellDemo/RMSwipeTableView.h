//
//  RMSwipeTableViewController.h
//  RMSwipeTableView
//
//  Created by Rune Madsen on 2012-11-24.
//  Copyright (c) 2012 The App Boutique. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMSwipeTableViewDataSource.h"
#import "RMSwipeTableViewCell.h"

@interface RMSwipeTableView : UITableView <RMSwipeTableViewDataSource, RMSwipeTableViewCellDelegate>

@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, assign) IBOutlet id<RMSwipeTableViewDataSource> swipeTableDataSource;

//Pour la droite
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;

@end
