//
//  AddCreditViewController.m
//  firststep
//
//  Created by Donovan Charpin on 08/07/13.
//
//

#import "AddCreditViewController.h"


@interface AddCreditViewController ()

@end

@implementation AddCreditViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
}


- (IBAction)shareOnFacebook:(id)sender {    
    SLComposeViewController *fbController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [fbController setInitialText:@"Firstep est une application de rencontre à découvrir sur l'appStore à découvrir"];
    [fbController addImage:[UIImage imageNamed:@"logo114px.png"]];
        
    [self presentViewController:fbController
                       animated:YES completion:nil];
}

@end
