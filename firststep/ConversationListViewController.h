//
//  ConversationListViewController.h
//  firststep
//
//  Created by Stéphane Bruckert on 9/7/13.
//
//

#import <UIKit/UIKit.h>
#import "RMSwipeTableView.h"
#import "RMSwipeTableViewDataSource.h"

@interface ConversationListViewController : UIViewController <RMSwipeTableViewCellDelegate, RMSwipeTableViewDataSource>

@property (nonatomic, assign) IBOutlet id<RMSwipeTableViewCellDelegate> delegate;

@property (nonatomic, retain) NSMutableArray* listUser;



- (IBAction)backButton:(id)sender;

//- (NSMutableArray*)retrieveUserList;
- (void)retrieveUserList;


@end
