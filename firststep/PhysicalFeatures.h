//
//  PhysicalFeatures.h
//
//  Created by Morgan BRASSEUR on 18/05/13.
//  Copyright (c) 2013 Morgan BRASSEUR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseStorable.h"

@interface PhysicalFeatures : NSObject <DatabaseStorable>

@end
