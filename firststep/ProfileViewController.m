//
//  ProfileViewController.m
//  firststep
//
//  Created by Stéphane Bruckert on 7/2/13.
//
//

#import "ProfileViewController.h"
#import "SegmentsController.h"
#import "Util.h"
#import "TabSecViewController.h"
#import "TabOneViewController.h"
#import "ItalyViewController.h"
#import "NSArray+PerformSelector.h"
#import <QuartzCore/QuartzCore.h>

@interface ProfileViewController ()
- (NSArray *)segmentViewControllers;
- (void)firstUserExperience;
@end

@implementation ProfileViewController {
    NSUInteger addCount;
}

@synthesize segmentsController, segmentedControl;

- (id)initWithUser:(User *)user {
    self = [super initWithNibName:@"ProfileViewController" bundle:nil];
    if (self) {
        _displayedUser = user;
    }
    return self;
}

- (IBAction)modificationMode:(id)sender {
    // Affichage des boutons
    // récupère les vues
    NSArray *subviews = segmentsController.viewControllers;
    
    if ([subviews count] == 0) return;
    
    if(_modeModificationAtivate == false){
        _modeModificationAtivate = true;
        [_buttonModification setImage:[UIImage imageNamed:@"termine.png"] forState:UIControlStateNormal];
        [_buttonModification setBackgroundImage:[UIImage imageNamed:@"termine.png"] forState:UIControlStateNormal];
        for (UIViewController *subview in subviews) {
            if ([subview isKindOfClass:[TabOneViewController class]]) {
                _modeMoficiationRectangle = [[UIView alloc]initWithFrame:CGRectMake(0, 50, 320, 40)];
                [_modeMoficiationRectangle setBackgroundColor:[UIColor colorWithRed:255/255.0f green:156/255.0f blue:0/255.0f alpha:1.0f]];
                _modeMoficiationRectangle.alpha = 0;
                [self.view addSubview:_modeMoficiationRectangle];
                _modeMoficiationlabel = [ [UILabel alloc ] initWithFrame:CGRectMake(0,0, 320, 42)];
                _modeMoficiationlabel.textAlignment =  UITextAlignmentCenter;
                _modeMoficiationlabel.textColor = [UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:1.0f];
                _modeMoficiationlabel.alpha = 0;
                _modeMoficiationlabel.backgroundColor = [UIColor clearColor];
                _modeMoficiationlabel.font = [UIFont fontWithName:@"Helvetica Neue Light" size:(17)];
                _modeMoficiationlabel.text = @"Mode modification";
                [_modeMoficiationRectangle addSubview:_modeMoficiationlabel];
                [UIView animateWithDuration:0.5f // This can be changed.
                 animations:^{
                    TabOneViewController* tab1 = (TabOneViewController*)subview;
                    tab1.buttonModification1.alpha = 1;
                    tab1.buttonModification2.alpha = 1;
                     _modeMoficiationRectangle.alpha = 1;
                     _modeMoficiationlabel.alpha = 1;
                 }];
            }
        }
    }else{
        _modeModificationAtivate = false;
        [_buttonModification setImage:[UIImage imageNamed:@"modifier.png"] forState:UIControlStateNormal];
        [_buttonModification setBackgroundImage:[UIImage imageNamed:@"modifier.png"] forState:UIControlStateNormal];
        for (UIViewController *subview in subviews) {
            if ([subview isKindOfClass:[TabOneViewController class]]) {
                [UIView animateWithDuration:0.5f // This can be changed.
                 animations:^{
                    TabOneViewController* tab1 = (TabOneViewController*)subview;
                    tab1.buttonModification1.alpha = 0;
                    tab1.buttonModification2.alpha = 0;
                     _modeMoficiationRectangle.alpha = 0;
                     _modeMoficiationlabel.alpha = 0;
                 }completion:^(BOOL finished){
                     [_modeMoficiationRectangle removeFromSuperview];
                     [_modeMoficiationlabel removeFromSuperview];
                 }];
            }
        }


    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    _modeModificationAtivate = false;

    return self;
}

- (void)viewDidLoad
{

    [super viewDidLoad];
    addCount = 0;
    [self updateSelectedSegmentLabel];
    
    //Cover
    self.profileBgImageView.image = [UIImage imageNamed:@"running.jpg"];
    self.profileBgImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.overlayView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.2f];

    FBProfilePictureView* userProfileImage = _displayedUser._profileImage;
    
    /* Affichage de la photo */
    CALayer *layer = [Util getImage:[self getUserImageFromFBView:userProfileImage]
                     withDimensions:CGRectMake(20, 60, 110, 110)
                surroundedWithColor:[Util getWhiteColor]
                         withRadius:55];
    
    [[scroller layer] addSublayer:layer];

    [scroller setScrollEnabled:YES];
    [scroller setContentSize:CGSizeMake(320, 560)];
    
    _name.text = _displayedUser._firstName;
    _city.text = _displayedUser._city;

        
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
    } else {
        // code for 3.5-inch screen
    }
    
    // ReturnButton
    UIButton *imgReturn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    imgReturn.frame = CGRectMake(30, 30, 30, 30); // position in the parent view and set the size of the button
    [imgReturn setBackgroundImage:[UIImage imageNamed:@"backButton"] forState:UIControlStateNormal];
    [imgReturn addTarget:self action:@selector(returnToFirstView:) forControlEvents:UIControlEventTouchUpInside];
    
    //Ajout tabs
    
    NSArray * viewControllers = [self segmentViewControllers];
    
    UINavigationController * navigationController = [[UINavigationController alloc] init];

    self.segmentsController = [[SegmentsController alloc] initWithNavigationController:navigationController viewControllers:viewControllers];
    navigationController.navigationBarHidden = YES;
    
    self.segmentedControl = [[SDSegmentedControl alloc] initWithItems:[viewControllers arrayByPerformingSelector:@selector(title)]];
    
    [self.segmentedControl addTarget:self.segmentsController
                              action:@selector(indexDidChangeForSegmentedControl:)
                    forControlEvents:UIControlEventValueChanged];
    self.segmentedControl.frame = CGRectMake(0, 0, 320, 40);
    [self firstUserExperience];
    
    [_tabView addSubview:navigationController.view];
    [navigationController.view addSubview:self.segmentedControl];

}

-(void)viewWillAppear:(BOOL)animated {
    
    self.navigationController.navigationBarHidden = YES; //suppression barre
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage*)getUserImageFromFBView:(FBProfilePictureView*)userProfileImage {
    for (NSObject *obj in [userProfileImage subviews]) {
        if ([obj isMemberOfClass:[UIImageView class]]) {
            UIImageView *objImg = (UIImageView *)obj;
            return objImg.image;
        }
    }
    return nil;
}

- (void)updateSelectedSegmentLabel
{
    self.selectedSegmentLabel.font = [UIFont boldSystemFontOfSize:self.selectedSegmentLabel.font.pointSize];
    
    self.selectedSegmentLabel.text = [NSString stringWithFormat:@"%d", self.segmentedControl.selectedSegmentIndex];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void)
                   {
                       self.selectedSegmentLabel.font = [UIFont systemFontOfSize:self.selectedSegmentLabel.font.pointSize];
                   });
}

- (IBAction)segmentDidChange:(id)sender
{
    [self updateSelectedSegmentLabel];
}

- (IBAction)backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSArray *)segmentViewControllers {
    ItalyViewController * tab3     = [[ItalyViewController alloc] initWithNibName:@"ItalyViewController" bundle:nil];
    TabSecViewController * tab2 = [[TabSecViewController alloc] initWithNibName:@"TabSecViewController" bundle:nil];
    TabOneViewController * tab1 = [[TabOneViewController alloc] initWithNibName:@"TabOneViewController" bundle:nil];
    
    tab3._displayedUser = _displayedUser;
    tab2._displayedUser = _displayedUser;
    tab1._displayedUser = _displayedUser;
    
    NSArray * viewControllers = [NSArray arrayWithObjects:tab1, tab3, tab2, nil];
    
    return viewControllers;
}

- (void)firstUserExperience {
    self.segmentedControl.selectedSegmentIndex = 0;
    [self.segmentsController indexDidChangeForSegmentedControl:self.segmentedControl];
}

@end
