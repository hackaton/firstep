//
//  ConversationListViewController.m
//  firststep
//
//  Created by Stéphane Bruckert on 9/7/13.
//
//

#import "ConversationListViewController.h"
#import "RMSwipeTableView.h"
#import "ChatViewController.h"
#import "AppDelegate.h"
#import "User.h"

@interface ConversationListViewController ()
{
    IBOutlet RMSwipeTableView* swipeTable; //RMSwipeTableView
}
@end

@implementation ConversationListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [swipeTable reloadData];
    _delegate = swipeTable;
    self.listUser = [[NSMutableArray alloc] init];
    [self retrieveUserList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    NSIndexPath *selectedIndex = [swipeTable indexPathForSelectedRow];
    [swipeTable deselectRowAtIndexPath:selectedIndex animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
}

- (IBAction)backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - RMSwipeTableViewDataSource implementation

- (NSInteger)rowsForTable:(RMSwipeTableView *)tableView
{
    NSLog(@"4");
    return [[tableView array] count];
}

- (NSMutableArray *)tableView:(RMSwipeTableView *)tableView dataForRow:(NSInteger)row
{
    NSLog(@"tableView:dataForRox");
    NSLog(@"5");
    return [[tableView array] objectAtIndex:row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Selected");
    NSLog(@"6");
    ChatViewController *view=[[ChatViewController alloc] initWithNibName:@"ChatViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:YES];
    [tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
}

// NSMutableArray*
- (void)retrieveUserList{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    // On récupère les id des user pour la liste de conversation
    PFQuery *query = [PFQuery queryWithClassName:@"Wink"];
    [query whereKey:@"idUserSender" equalTo:appDelegate.connectedUser._idFacebook];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            // Do something with the found objects
            for (PFObject *object in objects) {
                User * newUser = [[User alloc] initFromDatabase:[object objectForKey:@"idUserReceiver"]];
                [self.listUser addObject: newUser];
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];

    PFQuery *query2 = [PFQuery queryWithClassName:@"Wink"];
    [query2 whereKey:@"idUserReceiver" equalTo:appDelegate.connectedUser._idFacebook];
    [query2 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            // Do something with the found objects
            for (PFObject *object in objects) {
                User * newUser = [[User alloc] initFromDatabase:[object objectForKey:@"idUserSender"]];
                [self.listUser addObject: newUser];
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    NSLog(@"%@", self.listUser);

}


@end
