//
//  MyViewController1.h
//  SegmentedControlExample
//
//  Created by Marcus Crafter on 24/05/10.
//  Copyright 2010 Red Artisan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface ItalyViewController : UIViewController {
    UIImageView      * picture;
    User* _displayedUser;
}

@property (nonatomic, retain) IBOutlet UIImageView * picture;

@property (nonatomic, retain) User * _displayedUser;

@end
