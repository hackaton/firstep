//
//  TabSecViewController.m
//  firststep
//
//  Created by Stéphane Bruckert on 7/25/13.
//
//

#import "TabSecViewController.h"
#import "User.h"

@implementation TabSecViewController {
}

@synthesize _displayedUser;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"En commun";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
